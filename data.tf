####
# Policy
####

data "aws_iam_policy_document" "admin" {
  count = var.policy_content == null && var.policy_allow_scope == "ADMIN" ? 1 : 0

  statement {
    sid = "AllowAdmin"

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "secretsmanager:*",
    ]

    resources = [
      aws_secretsmanager_secret.this.arn,
    ]
  }
}

data "aws_iam_policy_document" "read_write" {
  count = var.policy_content == null && var.policy_allow_scope == "READ_WRITE" ? 1 : 0

  statement {
    sid = "AllowReadWrite"

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "secretsmanager:UntagResource",
      "secretsmanager:DescribeSecret",
      "secretsmanager:PutSecretValue",
      "secretsmanager:CreateSecret",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:UpdateSecret",
      "secretsmanager:GetRandomPassword",
      "secretsmanager:GetResourcePolicy",
      "secretsmanager:GetSecretValue",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:RestoreSecret",
      "secretsmanager:RotateSecret",
      "secretsmanager:UpdateSecretVersionStage",
      "secretsmanager:RemoveRegionsFromReplication",
      "secretsmanager:ListSecrets",
      "secretsmanager:TagResource"
    ]

    resources = [
      aws_secretsmanager_secret.this.arn,
    ]
  }
}

data "aws_iam_policy_document" "read" {
  count = var.policy_content == null && var.policy_allow_scope == "READ" ? 1 : 0

  statement {
    sid = "AllowRead"

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetRandomPassword",
      "secretsmanager:GetResourcePolicy",
      "secretsmanager:GetSecretValue",
      "secretsmanager:ListSecrets",
    ]

    resources = [
      aws_secretsmanager_secret.this.arn,
    ]
  }
}
